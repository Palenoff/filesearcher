﻿using System;

namespace FileSearch
{
    public class CurrentPathChangedArgs : EventArgs
    {
        public string Path { get; set; }
        public CurrentPathChangedArgs(string path)
        {
            Path = path;
        }
    }
}