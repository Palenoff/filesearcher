﻿using FileSearchLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace FileSearch
{
    /// <summary>
    /// Класс основной формы.
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// Количество секунд в таймере.
        /// </summary>
        int _seconds = 0;
        /// <summary>
        /// Объект, управляющий синхронизацией задач поиска файлов.
        /// </summary>
        ManualResetEvent tasksDispatcher = new ManualResetEvent(false);
        /// <summary>
        /// Объект, останавливающий задачу поиска файлов.
        /// </summary>
        CancellationTokenSource cancellationTokenSource = null;
        /// <summary>
        /// Токен, останавливающий задачу поиска файлов.
        /// </summary>
        CancellationToken cancellationToken;

        public MainForm()
        {
            InitializeComponent();
            FileSearcher.CurrentPathChanged += new EventHandler<CurrentPathChangedArgs>(FIlesTreeView_CreateFileNode);
        }

        private void NameCB_CheckStateChanged(object sender, EventArgs e)
        {
            NameTemplateTB.Enabled = NameTemplateCB.Checked;
        }

        private void TextCB_CheckStateChanged(object sender, EventArgs e)
        {
            TextToMatchRTB.Enabled = MatchTextCB.Checked;
        }

        private async void SearchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (SearchBtn.Text == "Поиск")
                {
                    if (NameTemplateCB.Checked && NameTemplateTB.Text == "")
                        throw new ArgumentNullException("NameTemplateTB.Text", "Поиск по незаданному шаблону невозможен.\nЕсли Вы не хотите искать файлы по шаблону имени, просто снимите флажок с пункта \"Шаблон имени\""); ;
                    if (MatchTextCB.Checked && TextToMatchRTB.Text == "")
                        throw new ArgumentNullException("TextToMatchRTB.Text", "Поиск по пустому тексту невозможен.\nЕсли Вы не хотите искать файлы по совпадению на внутреннее содержание текста, просто снимите флажок с пункта \"Текст\"");
                    if (!NameTemplateCB.Checked && !MatchTextCB.Checked)
                        return;
                    tasksDispatcher.Reset();
                    SearchBtn.Text = "Остановить";
                    PauseBtn.Enabled = true;
                    ResumeBtn.Enabled = false;
                    FileLbl.Text = "";
                    ProcessedFilesLbl.Text = "";
                    TimeLbl.Text = "";
                    FIlesTreeView.Nodes.Clear();
                    string nt = NameTemplateTB.Text, ttm = TextToMatchRTB.Text;
                    cancellationTokenSource = new CancellationTokenSource();
                    cancellationToken = cancellationTokenSource.Token;
                    FileSearcher.ResetFilesProcessed();
                    ResetTimer();
                    TimeLbl.Text = new DateTime().AddSeconds(_seconds).ToString("mm:ss");
                    FIlesTreeView.Nodes.Add(DirPathLbl.Text);
                    FileSearchTimer.Start();
                    tasksDispatcher.Set();

                    bool search_name_completed = !NameTemplateCB.Checked, match_text_completed = !MatchTextCB.Checked;


                    var current_file = new Progress<string>(file => FileLbl.Text = file);
                    var file_count = new Progress<string>(filesprocessed => ProcessedFilesLbl.Text = filesprocessed);
                    var search_name_completed_progress = new Progress<bool>(flag => search_name_completed = flag);
                    var match_text_completed_progress = new Progress<bool>(flag => match_text_completed = flag);

                    if (NameTemplateCB.Checked)
                        await Task.Factory.StartNew(() => FileSearcher.Search(DirPathLbl.Text, nt, FileSearcher.SearchName, current_file, file_count, search_name_completed_progress, tasksDispatcher, cancellationToken), TaskCreationOptions.LongRunning);
                    if (MatchTextCB.Checked)
                        await Task.Factory.StartNew(() => FileSearcher.Search(DirPathLbl.Text, ttm, FileSearcher.MatchText, current_file, file_count, match_text_completed_progress, tasksDispatcher, cancellationToken), TaskCreationOptions.LongRunning);

                    if (search_name_completed && match_text_completed)
                    {
                        FileSearchTimer.Stop();
                        SearchBtn.Text = "Поиск";
                        PauseBtn.Enabled = false;
                        ResumeBtn.Enabled = false;
                        MessageBox.Show("Поиск завершён!", "Поиск файлов", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (SearchBtn.Text == "Остановить")
                {
                    cancellationTokenSource.Cancel();
                    PauseBtn.Enabled = false;
                    ResumeBtn.Enabled = false;
                    FileSearchTimer.Stop();
                    SearchBtn.Text = "Поиск";
                    MessageBox.Show("Поиск остановлен!", "Поиск файлов", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка поиска файлов", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        /// <summary>
        /// Перезапускает таймер.
        /// </summary>
        private void ResetTimer()
        {
            _seconds = 0;
        }

        private void FileSearchTimer_Tick(object sender, EventArgs e)
        {
            _seconds++;
            TimeLbl.Text = new DateTime().AddSeconds(_seconds).ToString("mm:ss");
        }

        private void PauseBtn_Click(object sender, EventArgs e)
        {
            tasksDispatcher.Reset();
            FileSearchTimer.Stop();
            PauseBtn.Enabled = false;
            ResumeBtn.Enabled = true;
            MessageBox.Show("Поиск приостановлен!", "Поиск файлов", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ResumeBtn_Click(object sender, EventArgs e)
        {
            tasksDispatcher.Set();
            FileSearchTimer.Start();
            PauseBtn.Enabled = true;
            ResumeBtn.Enabled = false;
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                XmlWriter xmlWriter = XmlWriter.Create("Parametrs.xml");
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("root");
                xmlWriter.WriteStartElement("StartingDirectory");
                xmlWriter.WriteString(DirPathLbl.Text);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("NameTemplate");
                xmlWriter.WriteString(NameTemplateTB.Text);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("TextToMatch");
                xmlWriter.WriteString(TextToMatchRTB.Text);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
                xmlWriter.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка записи параметров", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Вызывается при возбуждении события FileSearchLibrary.CurrentPathChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FIlesTreeView_CreateFileNode(object sender, CurrentPathChangedArgs e)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        FIlesTreeView_CreateFileNode(sender, e);
                    });
                    return;
                }
                else
                {
                    string startdirectory = e.Path.Substring(0, DirPathLbl.Text.Length);
                    List<string> startfolders = startdirectory.Split('\\').ToList<string>();
                    startfolders.RemoveAll(x => x == "");
                    int index = startfolders.Count;
                    string[] folders = e.Path.Split('\\');
                    TreeNode node = FIlesTreeView.TopNode;
                    for (int i = index; i < folders.Length; i++)
                    {
                        if (!node.Nodes.ContainsKey(folders[i]))
                            node.Nodes.Add(folders[i], folders[i]);
                        node = node.Nodes[folders[i]];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка обработки дерева", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("Parametrs.xml");
                XmlElement xRoot = xDoc.DocumentElement;
                foreach (XmlNode node in xRoot.ChildNodes)
                {
                    if (node.Name == "StartingDirectory")
                        DirPathLbl.Text = node.InnerText;
                    if (node.Name == "NameTemplate")
                        NameTemplateTB.Text = node.InnerText;
                    if (node.Name == "TextToMatch")
                        TextToMatchRTB.Text = node.InnerText;
                }
            }
            catch (XmlException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка чтения параметров", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show("Файл с критериями не найден, так что критерии придётся написать ручками:(", "Ошибка чтения файла", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OpenDirBtn_Click(object sender, EventArgs e)
        {
            try
            {
                using (FolderBrowserDialog FBD = new FolderBrowserDialog())
                {
                    DialogResult result = FBD.ShowDialog();
                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(FBD.SelectedPath))
                    {
                        DirPathLbl.Text = FBD.SelectedPath;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка загрузки директории", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
