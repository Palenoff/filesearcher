﻿namespace FileSearch
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FIlesTreeView = new System.Windows.Forms.TreeView();
            this.FilesLbl = new System.Windows.Forms.Label();
            this.DirLbl = new System.Windows.Forms.Label();
            this.NameTemplateCB = new System.Windows.Forms.CheckBox();
            this.MatchTextCB = new System.Windows.Forms.CheckBox();
            this.NameTemplateTB = new System.Windows.Forms.TextBox();
            this.TextToMatchRTB = new System.Windows.Forms.RichTextBox();
            this.StateGB = new System.Windows.Forms.GroupBox();
            this.TimeLbl = new System.Windows.Forms.Label();
            this.TimeLblLbl = new System.Windows.Forms.Label();
            this.ProcessedFilesLbl = new System.Windows.Forms.Label();
            this.ProcessedFilesLblLbl = new System.Windows.Forms.Label();
            this.FileLbl = new System.Windows.Forms.Label();
            this.FileLblLbl = new System.Windows.Forms.Label();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.DirPathLbl = new System.Windows.Forms.Label();
            this.FileSearchTimer = new System.Windows.Forms.Timer(this.components);
            this.PauseBtn = new System.Windows.Forms.Button();
            this.ResumeBtn = new System.Windows.Forms.Button();
            this.OpenDirBtn = new System.Windows.Forms.Button();
            this.StateGB.SuspendLayout();
            this.SuspendLayout();
            // 
            // FIlesTreeView
            // 
            this.FIlesTreeView.Location = new System.Drawing.Point(534, 78);
            this.FIlesTreeView.Name = "FIlesTreeView";
            this.FIlesTreeView.Size = new System.Drawing.Size(239, 322);
            this.FIlesTreeView.TabIndex = 0;
            // 
            // FilesLbl
            // 
            this.FilesLbl.AutoSize = true;
            this.FilesLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FilesLbl.Location = new System.Drawing.Point(530, 22);
            this.FilesLbl.Name = "FilesLbl";
            this.FilesLbl.Size = new System.Drawing.Size(181, 24);
            this.FilesLbl.TabIndex = 1;
            this.FilesLbl.Text = "Найденные файлы:";
            // 
            // DirLbl
            // 
            this.DirLbl.AutoSize = true;
            this.DirLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DirLbl.Location = new System.Drawing.Point(14, 78);
            this.DirLbl.Name = "DirLbl";
            this.DirLbl.Size = new System.Drawing.Size(93, 17);
            this.DirLbl.TabIndex = 3;
            this.DirLbl.Text = "Директория:";
            // 
            // NameTemplateCB
            // 
            this.NameTemplateCB.AutoSize = true;
            this.NameTemplateCB.Checked = true;
            this.NameTemplateCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.NameTemplateCB.Location = new System.Drawing.Point(17, 124);
            this.NameTemplateCB.Name = "NameTemplateCB";
            this.NameTemplateCB.Size = new System.Drawing.Size(103, 17);
            this.NameTemplateCB.TabIndex = 7;
            this.NameTemplateCB.Text = "Шаблон имени:";
            this.NameTemplateCB.UseVisualStyleBackColor = true;
            this.NameTemplateCB.CheckStateChanged += new System.EventHandler(this.NameCB_CheckStateChanged);
            // 
            // MatchTextCB
            // 
            this.MatchTextCB.AutoSize = true;
            this.MatchTextCB.Checked = true;
            this.MatchTextCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MatchTextCB.Location = new System.Drawing.Point(17, 158);
            this.MatchTextCB.Name = "MatchTextCB";
            this.MatchTextCB.Size = new System.Drawing.Size(59, 17);
            this.MatchTextCB.TabIndex = 8;
            this.MatchTextCB.Text = "Текст:";
            this.MatchTextCB.UseVisualStyleBackColor = true;
            this.MatchTextCB.CheckStateChanged += new System.EventHandler(this.TextCB_CheckStateChanged);
            // 
            // NameTemplateTB
            // 
            this.NameTemplateTB.Location = new System.Drawing.Point(126, 121);
            this.NameTemplateTB.Name = "NameTemplateTB";
            this.NameTemplateTB.Size = new System.Drawing.Size(100, 20);
            this.NameTemplateTB.TabIndex = 9;
            // 
            // TextToMatchRTB
            // 
            this.TextToMatchRTB.Location = new System.Drawing.Point(17, 181);
            this.TextToMatchRTB.Name = "TextToMatchRTB";
            this.TextToMatchRTB.Size = new System.Drawing.Size(473, 81);
            this.TextToMatchRTB.TabIndex = 10;
            this.TextToMatchRTB.Text = "";
            // 
            // StateGB
            // 
            this.StateGB.Controls.Add(this.TimeLbl);
            this.StateGB.Controls.Add(this.TimeLblLbl);
            this.StateGB.Controls.Add(this.ProcessedFilesLbl);
            this.StateGB.Controls.Add(this.ProcessedFilesLblLbl);
            this.StateGB.Controls.Add(this.FileLbl);
            this.StateGB.Controls.Add(this.FileLblLbl);
            this.StateGB.Location = new System.Drawing.Point(17, 293);
            this.StateGB.Name = "StateGB";
            this.StateGB.Size = new System.Drawing.Size(483, 107);
            this.StateGB.TabIndex = 11;
            this.StateGB.TabStop = false;
            this.StateGB.Text = "Состояние";
            // 
            // TimeLbl
            // 
            this.TimeLbl.AutoSize = true;
            this.TimeLbl.Location = new System.Drawing.Point(146, 76);
            this.TimeLbl.Name = "TimeLbl";
            this.TimeLbl.Size = new System.Drawing.Size(0, 13);
            this.TimeLbl.TabIndex = 5;
            // 
            // TimeLblLbl
            // 
            this.TimeLblLbl.AutoSize = true;
            this.TimeLblLbl.Location = new System.Drawing.Point(23, 76);
            this.TimeLblLbl.Name = "TimeLblLbl";
            this.TimeLblLbl.Size = new System.Drawing.Size(43, 13);
            this.TimeLblLbl.TabIndex = 4;
            this.TimeLblLbl.Text = "Время:";
            // 
            // ProcessedFilesLbl
            // 
            this.ProcessedFilesLbl.AutoSize = true;
            this.ProcessedFilesLbl.Location = new System.Drawing.Point(146, 54);
            this.ProcessedFilesLbl.Name = "ProcessedFilesLbl";
            this.ProcessedFilesLbl.Size = new System.Drawing.Size(0, 13);
            this.ProcessedFilesLbl.TabIndex = 3;
            // 
            // ProcessedFilesLblLbl
            // 
            this.ProcessedFilesLblLbl.AutoSize = true;
            this.ProcessedFilesLblLbl.Location = new System.Drawing.Point(23, 53);
            this.ProcessedFilesLblLbl.Name = "ProcessedFilesLblLbl";
            this.ProcessedFilesLblLbl.Size = new System.Drawing.Size(113, 13);
            this.ProcessedFilesLblLbl.TabIndex = 2;
            this.ProcessedFilesLblLbl.Text = "Файлов обработано:";
            // 
            // FileLbl
            // 
            this.FileLbl.AutoSize = true;
            this.FileLbl.Location = new System.Drawing.Point(146, 29);
            this.FileLbl.Name = "FileLbl";
            this.FileLbl.Size = new System.Drawing.Size(0, 13);
            this.FileLbl.TabIndex = 1;
            // 
            // FileLblLbl
            // 
            this.FileLblLbl.AutoSize = true;
            this.FileLblLbl.Location = new System.Drawing.Point(23, 29);
            this.FileLblLbl.Name = "FileLblLbl";
            this.FileLblLbl.Size = new System.Drawing.Size(39, 13);
            this.FileLblLbl.TabIndex = 0;
            this.FileLblLbl.Text = "Файл:";
            // 
            // SearchBtn
            // 
            this.SearchBtn.Location = new System.Drawing.Point(407, 415);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(79, 23);
            this.SearchBtn.TabIndex = 12;
            this.SearchBtn.Text = "Поиск";
            this.SearchBtn.UseVisualStyleBackColor = true;
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // DirPathLbl
            // 
            this.DirPathLbl.AutoSize = true;
            this.DirPathLbl.Location = new System.Drawing.Point(123, 82);
            this.DirPathLbl.Name = "DirPathLbl";
            this.DirPathLbl.Size = new System.Drawing.Size(0, 13);
            this.DirPathLbl.TabIndex = 13;
            // 
            // FileSearchTimer
            // 
            this.FileSearchTimer.Interval = 1000;
            this.FileSearchTimer.Tick += new System.EventHandler(this.FileSearchTimer_Tick);
            // 
            // PauseBtn
            // 
            this.PauseBtn.Enabled = false;
            this.PauseBtn.Location = new System.Drawing.Point(322, 415);
            this.PauseBtn.Name = "PauseBtn";
            this.PauseBtn.Size = new System.Drawing.Size(79, 23);
            this.PauseBtn.TabIndex = 14;
            this.PauseBtn.Text = "Пауза";
            this.PauseBtn.UseVisualStyleBackColor = true;
            this.PauseBtn.Click += new System.EventHandler(this.PauseBtn_Click);
            // 
            // ResumeBtn
            // 
            this.ResumeBtn.Enabled = false;
            this.ResumeBtn.Location = new System.Drawing.Point(237, 415);
            this.ResumeBtn.Name = "ResumeBtn";
            this.ResumeBtn.Size = new System.Drawing.Size(79, 23);
            this.ResumeBtn.TabIndex = 15;
            this.ResumeBtn.Text = "Продолжить";
            this.ResumeBtn.UseVisualStyleBackColor = true;
            this.ResumeBtn.Click += new System.EventHandler(this.ResumeBtn_Click);
            // 
            // OpenDirBtn
            // 
            this.OpenDirBtn.Location = new System.Drawing.Point(322, 78);
            this.OpenDirBtn.Name = "OpenDirBtn";
            this.OpenDirBtn.Size = new System.Drawing.Size(138, 23);
            this.OpenDirBtn.TabIndex = 16;
            this.OpenDirBtn.Text = "Открыть директорию";
            this.OpenDirBtn.UseVisualStyleBackColor = true;
            this.OpenDirBtn.Click += new System.EventHandler(this.OpenDirBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.OpenDirBtn);
            this.Controls.Add(this.ResumeBtn);
            this.Controls.Add(this.PauseBtn);
            this.Controls.Add(this.DirPathLbl);
            this.Controls.Add(this.SearchBtn);
            this.Controls.Add(this.StateGB);
            this.Controls.Add(this.TextToMatchRTB);
            this.Controls.Add(this.NameTemplateTB);
            this.Controls.Add(this.MatchTextCB);
            this.Controls.Add(this.NameTemplateCB);
            this.Controls.Add(this.DirLbl);
            this.Controls.Add(this.FilesLbl);
            this.Controls.Add(this.FIlesTreeView);
            this.Name = "MainForm";
            this.Text = "Поиск файлов и текста в них";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.StateGB.ResumeLayout(false);
            this.StateGB.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView FIlesTreeView;
        private System.Windows.Forms.Label FilesLbl;
        private System.Windows.Forms.Label DirLbl;
        private System.Windows.Forms.CheckBox NameTemplateCB;
        private System.Windows.Forms.CheckBox MatchTextCB;
        private System.Windows.Forms.TextBox NameTemplateTB;
        private System.Windows.Forms.RichTextBox TextToMatchRTB;
        private System.Windows.Forms.GroupBox StateGB;
        private System.Windows.Forms.Label TimeLblLbl;
        private System.Windows.Forms.Label ProcessedFilesLbl;
        private System.Windows.Forms.Label ProcessedFilesLblLbl;
        private System.Windows.Forms.Label FileLblLbl;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.Label DirPathLbl;
        private System.Windows.Forms.Label FileLbl;
        private System.Windows.Forms.Timer FileSearchTimer;
        private System.Windows.Forms.Label TimeLbl;
        private System.Windows.Forms.Button PauseBtn;
        private System.Windows.Forms.Button ResumeBtn;
        private System.Windows.Forms.Button OpenDirBtn;
    }
}

