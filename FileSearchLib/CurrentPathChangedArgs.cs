﻿using System;

namespace FileSearchLib
{
    /// <summary>
    /// Предоставляет данные для события FileSearchLibrary.CurrentPathChanged
    /// </summary>
    public class CurrentPathChangedArgs : EventArgs
    {
        /// <summary>
        /// Путь к файлу, который должен быть отрисован в деревве.
        /// </summary>
        public string Path { get; set; }
        /// <summary>Инициализирует новый экземпляр класса FileSearchLibrary.CurrentPathChangedArgs.
        /// </summary>
        /// <param name="path">Путь к файлу, который должен быть отрисован в деревве.</param>
        public CurrentPathChangedArgs(string path)
        {
            Path = path;
        }
    }
}
