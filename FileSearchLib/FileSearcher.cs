﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace FileSearchLib
{
    public static class FileSearcher
    {
        /// <summary>
        /// Объект, блокирующий доступ к ресурсу для всех задач, кроме текущего.
        /// </summary>
        private static object locker = new object();
        /// <summary>
        /// Количество обработанных файлов.
        /// </summary>
        private static int filesprocessed = 0;
        /// <summary>
        /// Делегат, указывающий на способ обработки файла (поиск названия по шаблону или поиск на соответствие внутреннему содержимому).
        /// </summary>
        /// <param name="file">Путь к файлу.</param>
        /// <param name="t">Шаблон поиска.</param>
        public delegate void Act(string file, string t);
        /// <summary>
        /// Происходит при изменении (получении нового) пути, предназначенного для отрисовки в дереве.
        /// </summary>
        public static event EventHandler<CurrentPathChangedArgs> CurrentPathChanged;
        /// <summary>
        /// Вызывает событие FileSearchLibrary.CurrentPathChanged.
        /// </summary> 
        /// <param name="e">FileSearchLibrary.CurrentPathChangedArgs, содержащий данные события.</param>
        private static void OnCurrentPathChanged(CurrentPathChangedArgs e)
        {
            CurrentPathChanged?.Invoke(null, e);
        }
        /// <summary>
        /// Сбрасывает количество обработанных файлов.
        /// </summary>
        public static void ResetFilesProcessed()
        {
            filesprocessed = 0;
        }
        /// <summary>
        /// Прорисовывает файл в дереве от корня до листа для конкретного файла.
        /// </summary>
        /// <param name="file">Путь к файлу, который должен быть отрисован в деревве.</param>
        private static void AddFileToTree(string file)
        {
            OnCurrentPathChanged(new CurrentPathChangedArgs(Path.GetFullPath(file)));
        }
        /// <summary>
        /// Осуществляет поиск файлов по шаблону названия.
        /// </summary>
        /// <param name="file">Путь к файлу.</param>
        /// <param name="nametemplate">Шаблон имени файла.</param>        
        public static void SearchName(string file, string nametemplate)
        {
            if (Regex.IsMatch(file, nametemplate))
            {
                AddFileToTree(file);
            }
        }
        /// <summary>
        /// Осуществляет поиск файлов по соответствию на внутреннее содержимое.
        /// </summary>
        /// <param name="file">Путь к файлу.</param>
        /// <param name="nametemplate">Текст, по которому осуществляется поиск в файле.</param>  
        public static void MatchText(string file, string text)
        {
            using (StreamReader sr = File.OpenText(file))
            {
                string s = sr.ReadToEnd();
                if (s.Contains(text))
                    AddFileToTree(file);
            }
        }
        /// <summary>
        /// Осуществляет поиск файлов.
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <param name="text">Шаблон имени/текст для поиска</param>
        /// <param name="act">Способ поиска</param>
        /// <param name="currentfile">Отражение обрабатываемого файла</param>
        /// <param name="countfiles">Отражение количества обработанных файлов</param>
        /// <param name="completed">Отражение завершённости поиска</param>
        /// <param name="manualResetEvent">Объект, управляющий синхронизацией задач поиска файлов.</param>
        /// <param name="cancellationToken">Токен, останавливающий задачу поиска файлов.</param>
        public static void Search(string path, string text, Act act, IProgress<string> currentfile, IProgress<string> countfiles, IProgress<bool> completed, ManualResetEvent manualResetEvent, CancellationToken cancellationToken)
        {
            try
            {
                string[] files = Directory.GetFiles(path);
                foreach (string file in files)
                {
                    try
                    {
                        lock (locker)
                        {
                            filesprocessed++;
                            currentfile.Report(Path.GetFileName(file));
                            countfiles.Report(filesprocessed.ToString());
                        }
                        act(file, text);
                        manualResetEvent.WaitOne();
                        if (cancellationToken.IsCancellationRequested)
                        {
                            completed.Report(false);
                            return;
                        }

                    }
                    catch (UnauthorizedAccessException e)
                    {
                        continue;
                    }
                }
                string[] directories = Directory.GetDirectories(path);
                foreach (string directory in directories)
                {
                    Search(directory, text, act, currentfile, countfiles, completed, manualResetEvent, cancellationToken);
                    if (cancellationToken.IsCancellationRequested)
                        return;
                }
                completed.Report(true);
            }
            catch (UnauthorizedAccessException e)
            {
                return;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Ошибка построения дерева", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
